==================
⚡ Speedhack-rs ⚡
==================

Forked from  https://github.com/Hirtol/Speedhack-rs.

A simple utility to accomplish the same speedhack as can be found in CheatEngine,
without all the additional bloat.

Special Version for Trainer Base.

How to build
============

.. code:: bash

    cargo build --target x86_64-pc-windows-msvc --release
    cargo build --target i686-pc-windows-msvc --release
