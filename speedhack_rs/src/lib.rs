use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Mutex;
use std::time::Duration;

use anyhow::{Result};

use crate::speedhack::MANAGER;

mod speedhack;

static SHUTDOWN_FLAG: AtomicBool = AtomicBool::new(false);
/// Some games seem to double call DllMain somehow and need an additional guard here to prevent 2 threads running at the same time.
static LOAD_GUARD: Mutex<()> = Mutex::new(());

pub fn dll_attach(_hinst_dll: windows::Win32::Foundation::HMODULE) -> Result<()> {
    std::thread::sleep(Duration::from_millis(1000));

    let Ok(_lock) = LOAD_GUARD.try_lock() else {
        log::trace!("Failed to acquire lock, not the only thread running, stopping");
        return Ok(());
    };

    let speed_manager = &*speedhack::MANAGER;

    startup_routine()?;

    while !SHUTDOWN_FLAG.load(Ordering::Acquire) {
        {
            let mut manager = speed_manager.write().unwrap();
            manager.update_speed_from_master_value();
        }

        std::thread::sleep(Duration::from_millis(16));
    }

    Ok(())
}

pub fn dll_detach(_hinst_dll: windows::Win32::Foundation::HMODULE) -> Result<()> {
    SHUTDOWN_FLAG.store(true, Ordering::SeqCst);
    log::info!("Detached! {:?}", std::thread::current().id());

    Ok(())
}

fn startup_routine() -> anyhow::Result<()> {
    std::thread::spawn(move || {
        let manager = &*MANAGER;

        manager.write().unwrap().set_speed(1.0);

        std::thread::sleep(Duration::from_millis(300));

        let mut lock = manager.write().unwrap();
        lock.set_speed(1.0);
    });

    Ok(())
}
